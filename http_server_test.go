package http_server

import (
	"gitlab.com/flex_comp/comp"
	_ "gitlab.com/flex_comp/conf"
	_ "gitlab.com/flex_comp/log"
	_ "gitlab.com/flex_comp/util"
	"testing"
	"time"
)

func TestHttpServer(t *testing.T) {
	t.Run("all", func(t *testing.T) {
		_ = comp.Init(map[string]interface{}{"listen": "192.168.1.20", "id": 1, "env": "debug", "config": "./test/test.json"})

		chClose := make(chan bool)
		go func() {
			<-time.After(time.Minute * 10)
		}()

		go func() {
			<-time.After(time.Second)
			ch := Handle("/foo")
			for i := range ch {
				t.Log(string(i.raw))
				i.rsp <- i.raw
			}
		}()

		_ = comp.Start(chClose)
	})
}
