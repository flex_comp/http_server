package http_server

import "gitlab.com/flex_comp/util"

type Interactive struct {
	raw []byte
	rsp chan []byte
}

func (i *Interactive) Read(out interface{}) error {
	return util.JSONUnmarshal(i.raw, out)
}

func (i *Interactive) Write(in interface{}) error {
	b, err := util.JSONMarshal(in)
	if err != nil {
		return err
	}

	i.rsp <- b
	return nil
}
