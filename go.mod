module gitlab.com/flex_comp/http_server

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/valyala/fasthttp v1.29.0
	gitlab.com/flex_comp/comp v0.1.3
	gitlab.com/flex_comp/conf v0.1.0
	gitlab.com/flex_comp/log v0.0.0-20210729190650-519dfabf348e
	gitlab.com/flex_comp/util v0.0.0-20210813015552-fa5a0e253e1a
)
