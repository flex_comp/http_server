package http_server

import (
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/conf"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"net/http"
	"sync"
	"time"
)

var (
	HttpServerIns *HttpServer
)

type HttpServer struct {
	ip   string
	port int

	whiteList map[string]bool

	pathHandle    map[string]chan *Interactive
	pathHandleMtx sync.RWMutex
}

func init() {
	HttpServerIns = &HttpServer{
		whiteList:  make(map[string]bool),
		pathHandle: make(map[string]chan *Interactive),
	}

	_ = comp.RegComp(HttpServerIns)
}

func (h *HttpServer) Init(execArgs map[string]interface{}, _ ...interface{}) error {
	if ip, ok := execArgs["listen"]; ok {
		h.ip = util.ToString(ip)
	}

	h.port = util.ToInt(conf.Get("http.server.port"))
	arr := util.ToStringSlice(conf.Get("http.server.whitelist"))
	for _, ip := range arr {
		h.whiteList[ip] = true
	}

	return nil
}

func (h *HttpServer) Start(...interface{}) error {
	go h.listen()

	return nil
}

func (h *HttpServer) UnInit() {

}

func (h *HttpServer) Name() string {
	return "http-server"
}

func (h *HttpServer) listen() {
	addr := fmt.Sprintf("%s:%d", h.ip, h.port)

	for {
		err := fasthttp.ListenAndServe(addr, h.reqHandle)
		if err != nil {
			log.ErrorF("监听HTTP(%s)失败:%s, 1秒后重试", addr, err)
			<-time.After(time.Second)
			continue
		}

		return
	}
}

func (h *HttpServer) reqHandle(ctx *fasthttp.RequestCtx) {
	ip := ctx.RemoteIP()
	if len(h.whiteList) != 0 {
		if _, ok := h.whiteList[ip.String()]; !ok {
			ctx.Error("", http.StatusForbidden)
			return
		}
	}

	h.pathHandleMtx.RLock()
	defer h.pathHandleMtx.RUnlock()

	ch, ok := h.pathHandle[string(ctx.Path())]
	if !ok {
		return
	}

	i := &Interactive{
		raw: ctx.Request.Body(),
		rsp: make(chan []byte),
	}

	ch <- i
	ctx.Response.SetBody(<-i.rsp)
}

func Handle(path string) chan *Interactive {
	return HttpServerIns.Handle(path)
}

func (h *HttpServer) Handle(path string) chan *Interactive {
	i := make(chan *Interactive, 1024)
	h.pathHandleMtx.Lock()
	defer h.pathHandleMtx.Unlock()

	h.pathHandle[path] = i
	return i
}
